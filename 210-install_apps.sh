#!/usr/bin/env bash
set -e

echo "----------------------------------------------------------------------"
echo "First edit apps.txt file and select what applications you want"
echo "Don't continue if you haven't done it. Crtl+c to cancel"
echo "----------------------------------------------------------------------"
sleep 5s

echo "----------------------------------------------------------------------"
echo "     Installing apps..."
echo "----------------------------------------------------------------------"
ThisDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
yay -Syyuu --needed --noconfirm `sed -e '/^#/d' $ThisDir/apps.txt`
