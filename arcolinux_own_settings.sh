#!/bin/env bash
set -e




############################################################
############################################################
# START OF CONFIG
#
# To disable/enable anything write False/True on any field below
#
Compton=True
Fish=True
i3=True
Kvantum=True
Polybar=True
Termite=True
# END OF CONFIG
############################################################
############################################################




ThisDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ThisFile=$(basename -- "$0")
ThisPathAndFile=$(readlink -f $0)

############################################################
# Compton
############################################################
if [ $Compton == True ]; then
  file=~/.config/i3/compton.conf
  if neofetch | grep -q "VirtualBox Graphics Adapter"; then
    sed -i 's/^vsync = true;/#vsync = true;/' $file
  fi
fi




############################################################
# Fish
############################################################
if [ $Fish == True ]; then
  file=~/.config/fish/config.fish
  if hash fish 2>/dev/null; then
    if [ ! -d ~/.config/fish ]; then
      mkdir -p ~/.config/fish
    fi
    cp -Rvf $ThisDir/.config/fish/config.fish $file
    if hash fisher 2>/dev/null; then
      fisher add fishpkg/fish-prompt-metro
    fi
    if [ ! -d ~/.config/fish/conf.d ]; then
      mkdir -p ~/.config/fish/conf.d
    fi
  wget -O ~/.config/fish/conf.d/fish_command_timer.fish https://raw.githubusercontent.com/jichu4n/fish-command-timer/master/conf.d/fish_command_timer.fish
  fi
fi



############################################################
# i3
############################################################
if [ $i3 == True ]; then
  file=~/.config/i3/config

  # Change default browser
  xdg-settings set default-web-browser firefox.desktop

  # Change up, down left right in i3
  sed -i '/^#/! s/set \$up l/set \$up k/' ~/.config/i3/config
  sed -i '/^#/! s/set \$down k/set \$down j/' ~/.config/i3/config
  sed -i '/^#/! s/set \$left j/set \$left h/' ~/.config/i3/config
  sed -i '/^#/! s/set \$right semicolon/set \$right l/' ~/.config/i3/config

  # Change Split keys
  sed -i 's/mod+h/mod+period/' ~/.config/i3/config
  sed -i 's/mod+v/mod+minus/' ~/.config/i3/config

  # Assign Displays and workspaces
  # Get the first connected and treat it as internal
  internal=$(xrandr -q | grep '\Wconnect' | awk '{ print $1 }'| head -n 1)
  # Get second monitor
  second=$(xrandr -q | grep '\Wconnect' | awk '{ print $1 }'| sed -n 2p)
  file=~/.config/i3/config
  sed -i "s/^set \$firstMonitor.*/set \$firstMonitor $internal/" $file
  if [ ! -z $second ]; then
    sed -i "s/^set \$secondMonitor.*/set \$secondMonitor $second/" $file
  fi
  ## Workspaces
  sed -i "s/^workspace 2.*/workspace 2 output \$secondMonitor/" $file
  sed -i "s/^workspace 9.*/workspace 9 output \$firstMonitor/" $file
  sed -i "s/^workspace 10.*/workspace 10 output \$firstMonitor/" $file




  # Assiging apps to workspaces
  sed -i 's/^# Workspace 1 .*/# Workspace 1 non related/' $file
  sed -i '/.* → 1/d' $file
  sed -i '/.* → 2/d' $file
  if ! grep -q '[class="Firefox|firefox|Vivaldi-stable|Vivaldi-snapshot|Opera"]                       → 2' $file; then
    sed -i 's/^# Workspace 2.*/# Workspace 2 browser related\nassign [class="Firefox|firefox|Vivaldi-stable|Vivaldi-snapshot|Opera"]                       → 2\nfor_window [class="Firefox|Chromium"] focus/' $file
  fi
  sed -i '/.* → 3/d' $file
  sed -i 's/^# Workspace 3.*/# Workspace 3 text editor related\nassign [class="Atom|Code|VSCodium"]                                                  → 3/' $file
  sed -i 's/^#assign \[class="Vlc.*/assign [class="Vlc|vlc|Stremio"]          → 6/' $file
  sed -i '/^#assign \[class="Vmplayer.* /s/^#//' $file
  sed -i '/.* → 9/d' $file
  sed -i 's/^# Workspace 9.*/# Workspace 9 messaging clients related\nassign [class="TelegramDesktop|Whatsie|whats-app-nativ*"]                                   → 9/' $file
  sed -i '/.* → 10/d' $file
  sed -i 's/^# Workspace 10.*/# Workspace 10 Music related\nassign [class="Spotify|spotify"]                                             → 10/' $file
  sed -i '/^#for_window \[class="Spotify"\] move to workspace.* /s/^#//' $file
  sed -i '/^exec --no-startup-id thunar/s/^/#/' $file # Comment a line

  # Enable/Disable Autostart
  sed -i '/^#exec --no-startup-id blueberry-tray/s/^#//' $file # Uncomment a line
  sed -i '/^#exec --no-startup-id unclutter/s/^#//' $file # Uncomment a line
  sed -i '/^#exec --no-startup-id dropbox start/s/^#//' $file
  sed -i '/^exec --no-startup-id variety/s/^/#/' $file
  sed -i '/^exec --no-startup-id conky.*/s/^/#/' $file # Comment a line
  if hash polybar 2>/dev/null; then
    # Uncomment
    # sed -i '/^#exec_always --no-startup-id ~\/.config\/polybar\/launch.sh &/s/^#//' $file
    sed -i 's/^#exec_always --no-startup-id ~\/.config\/polybar\/launch.sh &/exec_always --no-startup-id ~\/.config\/polybar\/launch.sh &/' $file
  fi
  if ! grep -q "# PyWal at boot" $file; then
    sed -i '/polybar\/launch.sh/a \ \n# PyWal at boot\nexec --no-startup-id sleep 1; wal -i $(cat $HOME/.config/variety/wallpaper/wallpaper.jpg.txt) &' $file
  fi
  if ! grep -q "# XAutolock" $file; then
    sed -i '/polybar\/launch.sh/a \ \n# XAutolock\nexec --no-startup-id xautolock -time 10 -locker "~/.config/i3/scripts/i3exit.sh lock"' $file
  fi
  if ! grep -q "# Redshift" $file; then
    sed -i '/polybar\/launch.sh/a \ \n# Redshift\nexec --no-startup-id redshift' $file
  fi



  # Bindsym's
  sed -i -e '/.*dmenu.*/d' -e '/# gmrun/i # start dmenu\n' $file
  sed -i '/^# start dmenu/a bindsym \$mod+d exec --no-startup-id i3-dmenu-desktop --dmenu=\"dmenu -i -nb \'\'\$bg\'' -nf \'\'\$fg\'' -sb \'\'\$color9\'' -sf \'\'\$color15\'' -fn \'\'\NotoMonoRegular:bold:pixelsize=14\''\"' $file
  sed -i "/^# start dmenu/a bindsym \$mod+shift+d exec --no-startup-id dmenu_run -i -nb '\$bg' -nf '\$fg' -sb '\$color9' -sf '\$color15' -fn 'NotoMonoRegular:bold:pixelsize=14'" $file

  # Brightness
  sed -i 's/^bindsym XF86MonBrightnessUp.*/bindsym XF86MonBrightnessUp exec --no-startup-id light -A 10 \&\& notify-send --expire-time=20 "$(printf %.0f%% $(light -G))"  # increase screen brightness/' $file
  sed -i 's/^bindsym XF86MonBrightnessDown.*/bindsym XF86MonBrightnessDown exec --no-startup-id light -U 10 \&\& notify-send --expire-time=20 "$(printf %.0f%% $(light -G))" # decrease screen brightness/' $file


  # comment  i3-bar
  if hash polybar 2>/dev/null; then
    sed -i '/^bar {/,+71 s/^/#/' $file
  fi

  # Script of battery detect
  if [ ! -d ~/.scripts ]; then
    mkdir -p ~/.scripts
  fi
  cp -Rvu $ThisDir/.scripts/detect_battery.sh ~/.scripts/.
  if ! grep -q "# Battery detect for polybar" $file; then
    cat <<EOT >> $file

# Battery detect for polybar
exec_always --no-startup-id ~/.scripts/detect_battery.sh
EOT
  fi

  # Script to detect active network interface
  cp -Rvu $ThisDir/.scripts/detect_net_interface.sh ~/.scripts/.
  if ! grep -q "# Network detect for polybar" $file; then
    cat <<EOT >> $file

# Network detect for polybar
exec_always --no-startup-id ~/.scripts/detect_net_interface.sh
EOT
  fi

  # Script to switch displays
  if ! grep -q "# Script to switch displays" $file; then
    cat <<EOT >> $file

# Script to switch displays
bindcode \$mod+33 exec --no-startup-id ~/.scripts/screenlayout-toggle.sh
EOT
  fi
  cp -Rvu $ThisDir/.scripts/screenlayout-toggle.sh ~/.scripts/.

  # Script to enable touchpad
  cp -Rvu $ThisDir/.scripts/touchpad-enable.sh ~/.scripts/.
  if ! grep -q "# Enable touchpad" $file; then
    cat <<EOT >> $file

# Enable touchpad
exec_always --no-startup-id ~/.scripts/touchpad-enable.sh
EOT
  fi


  
  # Import colors from Xresources
  if ! grep -b $bg i3wm.color0 $file; then
    sed -i '/^client.placeholder.*/i set_from_resource $bg i3wm.color0' $file
    sed -i '/^client.placeholder.*/i set_from_resource $fg i3wm.color7' $file
    sed -i '/^client.placeholder.*/i set_from_resource $color1 i3wm.color1' $file
    sed -i '/^client.placeholder.*/i set_from_resource $color2 i3wm.color2' $file
    sed -i '/^client.placeholder.*/i set_from_resource $color3 i3wm.color3' $file
    sed -i '/^client.placeholder.*/i set_from_resource $color4 i3wm.color4' $file
    sed -i '/^client.placeholder.*/i set_from_resource $color5 i3wm.color5' $file
    sed -i '/^client.placeholder.*/i set_from_resource $color6 i3wm.color6' $file
    sed -i '/^client.placeholder.*/i set_from_resource $color7 i3wm.color7' $file
    sed -i '/^client.placeholder.*/i set_from_resource $color8 i3wm.color8' $file
    sed -i '/^client.placeholder.*/i set_from_resource $color9 i3wm.color9' $file
    sed -i '/^client.placeholder.*/i set_from_resource $color10 i3wm.color10' $file
    sed -i '/^client.placeholder.*/i set_from_resource $color11 i3wm.color11' $file
    sed -i '/^client.placeholder.*/i set_from_resource $color12 i3wm.color12' $file
    sed -i '/^client.placeholder.*/i set_from_resource $color13 i3wm.color13' $file
    sed -i '/^client.placeholder.*/i set_from_resource $color14 i3wm.color14' $file
    sed -i '/^client.placeholder.*/i set_from_resource $color15 i3wm.color15' $file
  fi


  # Floating windows
  if ! grep -q "class=GParted" $file; then
     sed -i '/Xfce4-taskmanager/a for_window [class="GParted"] floating enable' $file
  fi

  # Colors
  sed -i 's/^client.focused .*/client.focused          #4A4A4A $color6 #e5e5e5 #4A4A4A/' $file
fi




############################################################
# Kvantum
############################################################
if [ $Kvantum == True ]; then
  file=~/.config/qt5ct/qt5ct.conf
  sed -i 's/^style=.*/style=gtk2/' $file
fi




############################################################
# Polybar
############################################################
if hash polybar 2>/dev/null && [ $Polybar == True ]; then
  file=~/.config/polybar/config

  # Colors
  sed -i '0,/^background/{s/^background.*/background = ${xrdb:color0:#2F343F}/}' $file
  sed -i '0,/^foreground/{s/^foreground.*/foreground = ${xrdb:color7:#f3f4f5}/}' $file
  sed -i '0,/^label-mode-foreground/{s/^label-mode-foreground.*/label-mode-foreground = ${xrdb:color7:#000000}/}' $file
  sed -i '0,/^label-mode-background/{s/^label-mode-background.*/label-mode-background = ${xrdb:color0:#FFBB00}/}' $file
  sed -i '0,/^format-charging-underline =.*/{s/^format-charging-underline =.*/format-charging-underline = ${xrdb:color4:#a3c725}/}' $file
  sed -i '0,/^format-discharging-underline =.*/{s/^format-discharging-underline =.*/format-discharging-underline = ${self.format-charging-underline}/}' $file
  sed -i '/\[module\/date\]/,/label/!b;/format-prefix-foreground = .*/!b;s//format-prefix-foreground = ${xrdb:color1:#c1941a}/;:a;n;$!ba' $file
  sed -i '/\[module\/cpu2\]/,/label/!b;/format-prefix-foreground = .*/!b;s//format-prefix-foreground = ${xrdb:color2:#cd1f3f}/;:a;n;$!ba' $file
  sed -i '/\[module\/cpu2\]/,/label/!b;/format-underline = .*/!b;s//format-underline = ${xrdb:color2:#cd1f3f}/;:a;n;$!ba' $file

  # Height of all bars
  sed -i 's/^height.*/height = 20/' $file

  # Modules
  sed -i '0,/^modules-center/{s/modules-center.*/modules-center = date/}' $file
  sed -i '0,/^modules-right/{s/modules-right.*/modules-right = networkspeedup networkspeeddown filesystem battery memory2 cpu2/}' $file

  # Icons
  sed -i 's/^ws-icon-0.*/ws-icon-0 = 1;/' $file
  sed -i 's/^ws-icon-1.*/ws-icon-1 = 2;/' $file
  sed -i 's/^ws-icon-2.*/ws-icon-2 = 3;/' $file
  #sed -i 's/^ws-icon-3.*/ws-icon-0 = 1;/' $file
  #sed -i 's/^ws-icon-4.*/ws-icon-0 = 1;/' $file
  #sed -i 's/^ws-icon-5.*/ws-icon-0 = 1;/' $file
  #sed -i 's/^ws-icon-6.*/ws-icon-0 = 1;/' $file
  #sed -i 's/^ws-icon-7.*/ws-icon-0 = 1;/' $file
  #sed -i 's/^ws-icon-8.*/ws-icon-0 = 1;/' $file
  sed -i 's/^ws-icon-9.*/ws-icon-9 = 10;/' $file

  # Labels
  sed -i 's/label-focused = .*/label-focused = %icon%/' $file
  sed -i 's/label-focused-underline = .*/label-focused-underline = ${xrdb:color3:#6790eb}/' $file
  sed -i 's/label = Cpu %perce.*/label = %percentage:3%%/' $file
  sed -i 's/format = Mem <label>/format = <label>/' $file
  sed -i 's/^date = .*/; date = " %m-%d%"/' $file
  sed -i 's/^label-mounted = .*/label-mounted = %mountpoint% : %free%/' $file

  # Add filetype to the end if it does not exist
  if ! grep -q "; vim:ft=dosini" $file; then
    echo "; vim:ft=dosini" >> $file
  fi

fi



############################################################
# Termite
############################################################
if [ $Termite == True ]; then
  file=~/.config/termite/config
  sed -i '0,/font/{s/font = Monospace 12/font = Monospace 10/1}' $file
fi




############################################################
# Variety
############################################################
file=~/.config/variety/variety.conf
if [ -f $file ]; then
  # Stop automatic change
  sed -i 's/change_enabled = True/change_enabled = False/' $file
  # Activate sources
  sed -i 's/False|bing|/True|bing|/' $file
  sed -i 's/False|unsplash|/True|unsplash|/' $file
  # Hide icon
  sed -i 's/icon = Light/icon = None/' $file
fi




############################################################
# Vim
############################################################
file=~/.vimrc
if [ ! -f $file ]; then
  touch $file
fi
if [ -f $file ]; then
  if ! grep -q relativenumber $file; then
    echo '" Set numbers on the left side' >> $file
    echo "set relativenumber" >> $file
    echo "set number" >> $file
    echo '' >> $file
  fi
  if ! grep -q expandtab $file; then
    echo '' >> $file
    echo '" For 2 spaces on tabs' >> $file
    cat <<EOT >> $file
filetype indent on
" On pressing tab, insert 2 spaces
set expandtab
" show existing tab with 2 spaces width
set tabstop=2
set softtabstop=2
" when indenting with '>', use 2 spaces width
set shiftwidth=2

" Enable syntax and plugins
syntax enable
filetype plugin on
EOT
  fi
fi




############################################################
# .bashrc
############################################################
file=~/.bashrc




############################################################
# Python-wal
############################################################
file=~/.vim/colors/wal.vim
if [ ! -f $file ]; then
  if [ ! -d ~/.vim/colors ]; then
    mkdir -p ~/.vim/colors
  fi
  wget -c https://raw.githubusercontent.com/dylanaraps/wal/master/colors/wal.vim -O $file
fi




############################################################
# Script to .scripts
############################################################
if [ ! -d ~/.scripts ]; then
  mkdir ~/.scripts
fi
if [ ! $ThisPathAndFile == ~/.scripts/$ThisFile ]; then
  cp -Rvu $ThisPathAndFile ~/.scripts/
fi
