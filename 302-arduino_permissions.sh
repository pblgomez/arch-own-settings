#!/usr/bin/env bash
set -e

group=uucp
if ! getent group $group | grep &>/dev/null "\b${USER}\b"; then
  echo "----------------------------------------------------------------------"
  echo "     Adding User to groups to flash arduino"
  echo "----------------------------------------------------------------------"
  sudo usermod -a -G $group $USER
fi
