#!/usr/bin/env bash
  
DEVICE=$(ls /sys/class/power_supply/ | grep BAT | cut -c -4)

#DEVICEPROP=$(xinput list-props $DEVICE | grep "Tapping Enabled (" | cut -d "(" -f2 | cut -d ")" -f1)

#xinput set-prop $DEVICE $DEVICEPROP 1
echo $DEVICE
sed -i 's,battery = .*,battery = '"$DEVICE"',g' $HOME/.config/polybar/config
