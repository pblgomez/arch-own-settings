#!/usr/bin/env bash
set -e

file=~/.config/polybar/config
DEVICE=$(ip addr show | awk '/inet.*brd.*dynamic/{print $NF}')

sed -i '0,/^module-networkspeedup/{s/^interface =.*/interface = '"$DEVICE"'/}' $file
