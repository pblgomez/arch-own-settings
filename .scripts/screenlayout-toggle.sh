#!/usr/bin/env bash
set -e


# if we don't have a file, start at zero
if [ ! -f "/tmp/monitor_mode.dat" ] ; then
  monitor_mode="all"

# otherwise read the value from the file
else
  monitor_mode=`cat /tmp/monitor_mode.dat`
fi



# Get the first connected and treat it as internal
internal=$(xrandr -q | grep '\Wconnect' | awk '{ print $1 }'| head -n 1)

# Does an external HDMI exist?
for output in $(xrandr | grep '\Wconnected' | awk '{ print $1 }'); do
  if [[ $output =~ ^HDMI.*$ ]]; then
    external=$output
  fi
done

if [[ $external =~ ^HDMI.*$ ]]; then
  if [ $monitor_mode = "all" ]; then
    monitor_mode="external"
    xrandr --output $internal --off --output $external --auto
  elif [ $monitor_mode = "external" ]; then
    monitor_mode="clones"
    xrandr --output $internal --auto --output $external --auto --same-as $internal
  elif [ $monitor_mode = "clones" ]; then
    monitor_mode="all"
    xrandr --output $internal --auto --output $external --auto --above $internal
  fi
  pacmd set-card-profile 0 output:hdmi-stereo-extra1+input:analog-stereo
elif [[ ! $external =~ ^HDMI.*$ ]]; then
  xrandr --output $internal --auto --output $external --off || \ 
  xrandr --output $internal --auto --output HDMI-1 --off || \ 
  xrandr --output $internal --auto --output HDMI-2 --off

  # cmd el demonio no responde
  pacmd set-card-profile 0 output:analog-stereo+input:analog-stereo
fi

notify-send $monitor_mode
# Write variable to file 
echo "${monitor_mode}" > /tmp/monitor_mode.dat



# kill $(ps aux | grep 'volumeicon' | awk '{print $2}'); volumeicon
