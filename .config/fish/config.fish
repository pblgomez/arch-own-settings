set -x PATH $PATH /snap/bin
set -gx TERM linux
abbr df 'df -h -x tmpfs -x squashfs'
abbr update 'sudo pacman -Syu --noconfirm; yay -Syu --noconfirm; sudo snap refresh'
abbr free 'free -mt'

# GIT
alias gs='git status 2>/dev/null || yadm status'
#alias ga='git add 2>/dev/null || yadm add' 
abbr gpl 'git pull 2>/dev/null || yadm pull'
abbr gca 'git commit --amend -m'
abbr gps 'git push 2>/dev/null || yadm push'
function ga
  command git add $argv 2>/dev/null || yadm add $argv
end
function gc
  command git commit -m $argv 2>/dev/null || yadm commit -m $argv
end
function gco
  command git chekout 2>/dev/null || yadm checkout
end

## Pacman mirrors
alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"



#copy/paste all content of /etc/skel over to home folder - Beware
alias skel='cp -rf /etc/skel/.* ~'
#backup contents of /etc/skel to hidden backup folder in home/user
alias bupskel='cp -Rf /etc/skel ~/.skel-backup-(date +%Y.%m.%d-%H.%M.%S)'

# Own Settings
alias ownsettings='./.scripts/arcolinux_own_settings.sh'

# Import colorscheme from 'wal' asynchronously
# &   # Run the process in the background.
# ( ) # Hide shell job control messages.
# (cat ~/.cache/wal/sequences &)

# Alternative (blocks terminal for 0-3ms)
cat ~/.cache/wal/sequences

# To add support for TTYs this line can be optionally added.
# source ~/.cache/wal/colors-tty.sh



# Source fish commanf timer to get the executed time
source ~/.config/fish/conf.d/fish_command_timer.fish
